subroutine print_screen(a, n)
    implicit none
    integer :: n
    integer :: a(n, n)

    ! This function will print the game screen out
    print '(1x, 4I5)', a
    return
end subroutine print_screen

subroutine random_coord(x, y)
    integer :: x, y
    real :: randx, randy

    call RANDOM_NUMBER(randx)
    call RANDOM_NUMBER(randy)
    
    x = INT(randx*4)+1
    y = INT(randy*4)+1
    
    return
end subroutine random_coord

subroutine new_block(a, n)
    implicit none
    integer :: n, x, y
    integer :: a(n, n)

    do
        call random_coord(x, y)
        if(a(x,y).eq.0) then
            a(x, y) = 2
            exit
        end if
    end do
    return
end subroutine new_block

subroutine collapse(a, n)
    do row = 1, n
    do col= 0, n-1
    if(array(col, row).eq.array(col+1, row)) then
        array(col, row) = array(col, row)*2
    end if
    end do
    end do

end subroutine collapse

subroutine clear()
    call system("clear")
    return
end subroutine clear

program main
    implicit none
    integer, parameter :: n = 4
    integer, dimension(n, n) :: array = 0
    integer ::  row, col, num_blocks = 16
    character :: ch

    call RANDOM_SEED

    do
        call clear()
        call print_screen(array, n)
        read(*,*) ch
        select case (ch)
            case ('q')
                exit
            case ('a')

                continue
            case('d')
                continue
            case('w')
                continue
            case('s')
                continue
            case default
                if(num_blocks > 0) then
                    call new_block(array, n)
                    num_blocks = num_blocks-1
                end if
        end select
    end do

end program main

